<?php

class FileUploadBase

{
	public $fileName;
	public $fileTempName;
	public $fileSize;
	public $fileType;
	public $location;

	public function getFileName()
	{
		return $this->fileName;
	}

	public function getFileTempName()
	{
		return $this->fileTempName;
	}

	public function getFileSize()
	{
		return $this->fileSize;
	}

	public function getFileType()
	{
		return $this->fileType;
	}

	public function getFileLocation()
	{
		return $this->location;
	}

	public function getSliderFileLocation()
	{
		return $this->location = "C:\\xampp\htdocs\application.blog\uploads\\sliders\\".$this->fileName;
	}

	public function setPostFileLocation()
	{
		$this->location = "C:\\xampp\htdocs\application.blog\uploads\\posts\\".$this->fileName;
	}

	public function setSliderFileLocation()
	{
		$this->location = "C:\\xampp\htdocs\application.blog\uploads\\sliders\\".$this->fileName;
	}

	public function setImageFileLocation()
	{
		$this->location = "C:\\xampp\htdocs\application.blog\uploads\\images\\".$this->fileName;
	}

	public function fileTypeData()
	{
		$this->fileType = strtolower(pathinfo($this->location, PATHINFO_EXTENSION));
	}

	public function fileSizeData($data)
	{
		$this->fileSize = $data['image']['size'];
	}

	public function tempName($data)
	{
		$this->fileTempName = $data['image']['tmp_name'];
	}

	public function fileNameData($data)
	{
		$this->fileName = basename($data['image']['name']);
	}

	public function checkFile()
	{
		if( $this->getFileSize() > 500000 ){

			$fileSizeError = 'File is to large';
			return $fileSizeError;

		} else {

			if( file_exists($this->getFileLocation()) ){

				$fileError = 'File already exists';
				return $fileError;

			} else {

				if( ($this->getFileType() != 'jpg') && ($this->getFileType() != 'png') && ($this->getFileType() != 'jpeg') ){

					$fileTypeError = 'Only jpg, png and jpeg files are allowed';
					return $fileTypeError;

				} else {

					return true;

				}

			}

		}
	}

	public function uploadFileData()
	{
		move_uploaded_file($this->getFileTempName(), $this->getFileLocation());
	}
}

?>