<?php

class Post extends ActiveRecord

{
	static protected $db_table = "posts";

	public $id;
	public $user_id;
	public $title;
	public $text;
	public $published;
	public $image;
	public $created;
}

?>