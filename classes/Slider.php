<?php

class Slider extends ActiveRecord

{
	static protected $db_table = "sliders";

	public $id;
	public $filename;
	public $caption;
	public $published;
	public $image;
	public $url;
}

?>