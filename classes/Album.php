<?php

class Album extends ActiveRecord

{
	static protected $db_table = "albums";

	public $id;
	public $name;
	public $description;
	public $published;
}

?>