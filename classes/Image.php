<?php

class Image extends ActiveRecord

{
	static protected $db_table = "images";

	public $id;
	public $album_id;
	public $filename;
	public $description;
	public $published;
	public $image;
}

?>