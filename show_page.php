<?php

	include_once "includes/inc.globals.php";
	include_once "includes/inc.front.php";

	$pages = Page::fetchAll("published='yes'");

	$categories = Category::fetchAll("published='true'");

	$albums = Album::fetchAll("published='yes'");

	$pageId = $_REQUEST['id'];

	$pageData = Page::fetch("id", $pageId);

	$smarty->assign("pageData", $pageData);
	$smarty->assign("pages", $pages);
	$smarty->assign("categories", $categories);
	$smarty->assign("albums", $albums);
	$smarty->display("show_page.tpl");

?>