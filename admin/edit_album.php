<?php

	include_once "../includes/inc.admin.php";

	$albumId = $_REQUEST['id'];

	$album = Album::fetch("id", $albumId);

	$smarty->assign("check", "checked");
	$smarty->assign("album", $album);
	$smarty->display("admin/edit_album.tpl");
	
?>