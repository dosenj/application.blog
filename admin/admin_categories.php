<?php

	include_once "../includes/inc.admin.php";
	error_reporting(E_ALL & ~E_NOTICE);

	if(isset($_REQUEST['Action'])){

		$action = $_REQUEST['Action'];
		
		switch ($action) {

			case "delete":

				$delete_ids = $_REQUEST['delete'];
				$num_deleted = Category::deleteIds($delete_ids);
				$message = "Successfully deleted $num_deleted item(s).";

				break;

			case "add_category":

				$category_data = $_REQUEST['category'];

				if(empty($category_data['name'])){
					$category_name_error = "Name must be provided.";
					break;
				}

				if(empty($category_data['published'])){
					$category_published_error = "Status must be provided.";
					break;
				}

				if( ($category_data['name']) && ($category_data['published']) ){
					
					$category = Category::createFromArray($category_data);
					$record_data = $category->save();

					if($record_data){
						$message = "Successfully saved.";
					} else {
						$message = "Failed to save.";
					}

				}

				break;

			case "edit_category":
			
					$category_data = $_REQUEST['category'];

					if(empty($category_data['name'])){
						$category_name_error = "Name must be provided.";
						break;
					}

					if(empty($category_data['published'])){
						$category_published_error = "Status must be provided.";
						break;
					}

					if( ($category_data['name']) && ($category_data['published']) ){

						$category = Category::fetch("id", $category_data['id']);
						$category->updateFromArray($category_data);

						$record_data = $category->save();

						if($record_data){
							$message = "Successfully saved.";
						} else {
							$message = "Failed to save.";
						}

					}

					break;	
			
			default:

				echo "Default value";

				break;

		}

	}

	$categories = Category::fetchAll();

	$smarty->assign("categories", $categories);
	$smarty->assign("admin", $admin);			
	$smarty->display("admin/admin_categories.tpl");

?>