<?php

	include_once "../includes/inc.admin.php";

	if(isset($_REQUEST['Action'])){

		$action = $_REQUEST['Action'];

		switch ($action) {

			case "delete":

				$delete_ids = $_REQUEST['delete'];
				$num_deleted = Page::deleteIds($delete_ids);
				$message = "Successfully deleted $num_deleted item(s).";

				break;
			
			case "add_page":

				$page_data = $_REQUEST['page'];

				if(empty($page_data['title'])){
					$page_title_error = "Page title must be provided.";
					break;
				}

				if(empty($page_data['text'])){
					$page_content_error = "Page text must be provided.";
					break;
				}

				if(empty($page_data['published'])){
					$page_status_error = "Page status must be provided";
					break;
				}

				if( ($page_data['title']) && ($page_data['text']) && ($page_data['published']) ){

					$page = Page::createFromArray($page_data);
					$result = $page->save();

					if($result){
						$message="Successfully saved.";
					} else {
						$message="Failed to save.";
					}

				}

				break;

			case "edit_page":

				$page_data = $_REQUEST['page'];

				if(empty($page_data['title'])){
					$page_title_error = "Page title must be provided.";
					break;
				}

				if(empty($page_data['text'])){
					$page_content_error = "Page text must be provided.";
					break;
				}

				if(empty($page_data['published'])){
					$page_status_error = "Page status must be provided";
					break;
				}

				if( ($page_data['title']) && ($page_data['text']) && ($page_data['published']) ){

					$page = Page::fetch("id", $page_data['id']);
					$page->updateFromArray($page_data);

					$page_result = $page->save();

					if($page_result){
						$message = "Successfully saved.";
					} else {
						$message = "Failed to save.";
					}

				}

				break;

		}

	}

	$pages = Page::fetchAll();

	$smarty->assign("pages", $pages);
	$smarty->display("admin/admin_pages.tpl");

?>