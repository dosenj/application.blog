<?php

	include_once "../includes/inc.admin.php";

	$categories = Category::fetchAll();

	$smarty->assign("categories", $categories);

	$postId = $_REQUEST['id'];

	$post = Post::fetch("id", $postId);

	$postCategories = PostCategory::fetchAll("post_id='".$post->id."'");
	
	$smarty->assign("postCategories", $postCategories);

	$smarty->assign("post", $post);
	$smarty->assign("check", "checked");
	$smarty->display("admin/edit_post.tpl");

?>