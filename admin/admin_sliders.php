<?php

	include_once "../includes/inc.admin.php";

	if(isset($_REQUEST['Action'])){

		$action = $_REQUEST['Action'];

		switch ($action) {

			case "delete":

				$delete_ids = $_REQUEST['delete'];
				$num_deleted = Slider::deleteIds($delete_ids);
				$message = "Successfully deleted $num_deleted item(s).";

				foreach ($delete_ids as $key => $value) {
					
					$sliderData = Slider::fetch("id", $value);
					unlink("C:\\xampp\htdocs\application.blog\uploads\\sliders\\".$sliderData->image);

				}

				break;

			case "add_slider":

				$slider_data = $_REQUEST['slider'];

				$uploaded_data = new FileUploadBase();

				$uploaded_data->fileNameData($_FILES);
				$uploaded_data->tempName($_FILES);
				$uploaded_data->fileSizeData($_FILES);
				$uploaded_data->setSliderFileLocation();
				$uploaded_data->fileTypeData();

				$check = $uploaded_data->checkFile();

				if(empty($slider_data['filename'])){
					$slider_filename_error = "Slider name must be provided.";
					break;
				}

				if(empty($slider_data['caption'])){
					$slider_caption_error = "Slider caption must be provided.";
					break;
				}

				if(empty($slider_data['published'])){
					$slider_status_error = "Slider status must be provided.";
					break;
				}

				if( ($slider_data['filename']) && ($slider_data['caption']) && ($slider_data['published']) ){

					$uploaded_data->uploadFileData();

					$slider = Slider::createFromArray($slider_data);
					$slider->image = $uploaded_data->getFileName();
					$slider->url = $uploaded_data->getSliderFileLocation();
					$record_data = $slider->save();

					if($record_data){
						$message = "Successfully saved.";
					} else {
						$message = "Failed to save.";
					}

				}

				break;

			case "edit_slider":

				$slider_data = $_REQUEST['slider'];

				$uploaded_data = new FileUploadBase();

				$uploaded_data->fileNameData($_FILES);
				$uploaded_data->tempName($_FILES);
				$uploaded_data->fileSizeData($_FILES);
				$uploaded_data->setSliderFileLocation();
				$uploaded_data->fileTypeData();

				$check = $uploaded_data->checkFile();

				if(empty($slider_data['filename'])){
					$slider_filename_error = "Slider name must be provided.";
					break;
				}

				if(empty($slider_data['caption'])){
					$slider_caption_error = "Slider caption must be provided.";
					break;
				}

				if(empty($slider_data['published'])){
					$slider_status_error = "Slider status must be provided.";
					break;
				}

				if( ($slider_data['filename']) && ($slider_data['caption']) && ($slider_data['published']) ){

					if($check){
						$uploaded_data->uploadFileData();
					}

					$slider = Slider::fetch("id", $slider_data['id']);

					if( $check && !empty($uploaded_data->getFileName()) ){
						$slider->image = $uploaded_data->getFileName();
						$slider->url = $uploaded_data->getSliderFileLocation();
					}

					$slider->updateFromArray($slider_data);
					$record_data = $slider->save();

					if($record_data){
						$message = "Successfully saved.";
					} else {
						$message = "Failed to save.";
					}

				}

				break;
			
		}

	}

	$sliders = Slider::fetchAll();

	$smarty->assign("sliders", $sliders);
	$smarty->display("admin/admin_sliders.tpl");

?>