<?php

	include_once "../includes/inc.admin.php";

	$albums = Album::fetchAll();

	$imageId = $_REQUEST['id'];

	$image = Image::fetch("id", $imageId);

	$smarty->assign("albums", $albums);
	$smarty->assign("select", "selected");
	$smarty->assign("check", "checked");
	$smarty->assign("image", $image);
	$smarty->display("admin/edit_image.tpl");

?>