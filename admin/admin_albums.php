<?php

	include_once "../includes/inc.admin.php";

	if(isset($_REQUEST['Action'])){

		$action = $_REQUEST['Action'];

		switch ($action) {

			case "delete":

				$delete_ids = $_REQUEST['delete'];
				$num_deleted = Album::deleteIds($delete_ids);
				$message = "Successfully deleted $num_deleted item(s).";

				break;

			case "add_album":

				$album_data = $_REQUEST['album'];

				if(empty($album_data['name'])){
					$album_name_error = "Album name must be provided.";
					break;
				}

				if(empty($album_data['description'])){
					$album_description_error = "Album description must be provided.";
					break;
				}

				if(empty($album_data['published'])){
					$album_status_error = "Album status must be provided.";
					break;
				}

				if( ($album_data['name']) && ($album_data['description']) && ($album_data['published']) ){

					$album = Album::createFromArray($album_data);
					$record_data = $album->save();

					if($record_data){
						$message="Successfully saved.";
					} else {
						$message="Failed to save.";
					}

				}

				break;

			case "edit_album":

				$album_data = $_REQUEST['album'];

				if(empty($album_data['name'])){
					$album_name_error = "Album name must be provided.";
					break;
				}

				if(empty($album_data['description'])){
					$album_description_error = "Album description must be provided.";
					break;
				}

				if(empty($album_data['published'])){
					$album_status_error = "Album status must be provided.";
					break;
				}

				if( ($album_data['name']) && ($album_data['description']) && ($album_data['published']) ){

					$album = Album::fetch("id", $album_data['id']);
					$album->updateFromArray($album_data);

					$result = $album->save();

					if($result){
						$message="Successfully saved.";
					} else {
						$message="Failed to save.";
					}

				}

				break;
			
		}

	}

	$albums = Album::fetchAll();

	$smarty->assign("albums", $albums);
	$smarty->display("admin/admin_albums.tpl");

?>