<?php

	include_once "../includes/inc.admin.php";

	$id = $_REQUEST['id'];

	$category = Category::fetch("id", $id);

	$smarty->assign("category", $category);
	$smarty->assign("check", "checked");
	$smarty->display("admin/edit_category.tpl");

?>