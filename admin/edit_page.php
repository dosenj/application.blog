<?php

	include_once "../includes/inc.admin.php";

	$pageId = $_REQUEST['id'];

	$page = Page::fetch("id", $pageId);

	$smarty->assign("page", $page);
	$smarty->assign("check", "checked");
	$smarty->display("admin/edit_page.tpl");

?>