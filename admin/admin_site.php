<?php

	include_once "../includes/inc.admin.php";

	$pages = Page::fetchAll("published='yes'");

	$categories = Category::fetchAll("published='true'");

	$albums = Album::fetchAll("published='yes'");

	$sliders = Slider::fetchAll("published='yes'");

	$posts = Post::fetchAll("published='true'");

	$images = Image::fetchAll("published='yes'");

	$smarty->assign("pages", $pages);
	$smarty->assign("categories", $categories);
	$smarty->assign("albums", $albums);
	$smarty->assign("sliders", $sliders);
	$smarty->assign("posts", $posts);
	$smarty->assign("images", $images);

	$smarty->display("admin/admin_site.tpl");

?>