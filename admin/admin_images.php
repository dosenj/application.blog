<?php

	include_once "../includes/inc.admin.php";

	if(isset($_REQUEST['Action'])){

		$action = $_REQUEST['Action'];

		switch ($action) {

			case "delete":

				$delete_ids = $_REQUEST['delete'];
				$num_deleted = Image::deleteIds($delete_ids);
				$message = "Successfully deleted $num_deleted item(s).";

				foreach ($delete_ids as $key => $value) {
					
					$image = Image::fetch("id", $value);
					chmod("C:\\xampp\htdocs\application.blog\uploads\\images\\".$image->image, 0777);
					unlink("C:\\xampp\htdocs\application.blog\uploads\\images\\".$image->image);

				}

				break;

			case "add_image":
			
				$image_data = $_REQUEST['image'];

				$upload_image = new FileUploadBase();

				$upload_image->fileNameData($_FILES);
				$upload_image->tempName($_FILES);
				$upload_image->fileSizeData($_FILES);
				$upload_image->setImageFileLocation();
				$upload_image->fileTypeData();

				$check = $upload_image->checkFile();

				if(empty($image_data['filename'])){
					$image_filename_error = "Image filename must be provided.";
					break;
				}

				if(empty($image_data['description'])){
					$image_description_error = "Image description must be provided.";
					break;
				}

				if(empty($image_data['published'])){
					$image_status_error = "Image status must be provided.";
					break;
				}

				if(empty($image_data['album'])){
					$image_album_error = "Image album must be provided.";
					break;
				}

				if( ($image_data['filename']) && ($image_data['description']) && ($image_data['published']) && ($image_data['album']) ){

					$upload_image->uploadFileData();

					$image = Image::createFromArray($image_data);
					$image->album_id = $image_data['album'];
					$image->image = $upload_image->getFileName();
					$record_data = $image->save();

					if($record_data){
						$message = "Successfully saved.";
					} else {
						$message = "Failed to save.";
					}

				}

				break;	

			case "edit_image":

				$image_data = $_REQUEST['image'];

				$upload_image = new FileUploadBase();

				$upload_image->fileNameData($_FILES);
				$upload_image->tempName($_FILES);
				$upload_image->fileSizeData($_FILES);
				$upload_image->setImageFileLocation();
				$upload_image->fileTypeData();

				$check = $upload_image->checkFile();

				if(empty($image_data['filename'])){
					$image_filename_error = "Image filename must be provided.";
					break;
				}

				if(empty($image_data['description'])){
					$image_description_error = "Image description must be provided.";
					break;
				}

				if(empty($image_data['published'])){
					$image_status_error = "Image status must be provided.";
					break;
				}

				if(empty($image_data['album'])){
					$image_album_error = "Image album must be provided.";
					break;
				}

				if( ($image_data['filename']) && ($image_data['description']) && ($image_data['published']) && ($image_data['album']) ){

					if($check){
						$upload_image->uploadFileData();
					}

					$image = Image::fetch("id", $image_data['id']);

					if($check && !empty($upload_image->getFileName())){
						$image->image = $upload_image->getFileName();
					}

					$image->album_id = $image_data['album'];
					$image->updateFromArray($image_data);
					$result = $image->save();

					if($result){
						$message = "Successfully saved.";
					} else {
						$message = "Failed to save.";
					}

				}

				break;
			
		}

	}

	$images = Image::fetchAll();

	$smarty->assign("images", $images);
	$smarty->display("admin/admin_images.tpl");

?>