<?php

	include_once "../includes/inc.admin.php";

	$sliderId = $_REQUEST['id'];

	$slider = Slider::fetch("id", $sliderId);

	$smarty->assign("slider", $slider);
	$smarty->assign("check", "checked");
	$smarty->display("admin/edit_slider.tpl");

?>