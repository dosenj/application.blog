<?php

	include_once "../includes/inc.admin.php";

	if(isset($_REQUEST['Action'])){

		$action = $_REQUEST['Action'];

		switch ($action) {

			case "delete":

				$delete_ids = $_REQUEST['delete'];
				$num_deleted = Post::deleteIds($delete_ids);
				$message = "Successfully deleted $num_deleted item(s).";

				foreach ($delete_ids as $key => $value) {

					$postData = Post::fetch("id", $value);
					unlink("C:\\xampp\htdocs\application.blog\uploads\\posts\\".$postData->image);

				}

				break;
			
			case "add_post":

				$post_data = $_REQUEST['post'];

				$file_upload_data = new FileUploadBase();

				$file_upload_data->fileNameData($_FILES);
				$file_upload_data->tempName($_FILES);
				$file_upload_data->fileSizeData($_FILES);
				$file_upload_data->setPostFileLocation();
				$file_upload_data->fileTypeData();

				$check = $file_upload_data->checkFile();

				if(empty($post_data['title'])){
					$post_title_error = "Post title must be provided.";
					break;
				}

				if(empty($post_data['text'])){
					$post_text_error = "Post content must be provided.";
					break;
				}

				if(empty($post_data['published'])){
					$post_status_error = "Post status must be provided.";
					break;
				}

				if( ($post_data['title']) && ($post_data['text']) && ($post_data['published']) && $check ){

					$file_upload_data->uploadFileData();

					$userId = $logged_user->getAdminId();

					$post = Post::createFromArray($post_data);
					$post->user_id = $userId;
					$post->image = $file_upload_data->getFileName();
					$record_data = $post->save();

					if($record_data){
						$message = "Successfully saved.";
					} else {
						$message = "Failed to save.";
					}

				}

				break;

			case "edit_post":

				$post_data = $_REQUEST['post'];

				$file_upload_data = new FileUploadBase();

				$file_upload_data->fileNameData($_FILES);
				$file_upload_data->tempName($_FILES);
				$file_upload_data->fileSizeData($_FILES);
				$file_upload_data->setPostFileLocation();
				$file_upload_data->fileTypeData();

				$check = $file_upload_data->checkFile();

				if(empty($post_data['title'])){
					$post_title_error = "Post title must be provided.";
					break;
				}

				if(empty($post_data['text'])){
					$post_text_error = "Post content must be provided.";
					break;
				}

				if(empty($post_data['published'])){
					$post_status_error = "Post status must be provided.";
					break;
				}

				if( ($post_data['title']) && ($post_data['text']) && ($post_data['published']) ){

					if(!empty($_REQUEST['categories'])){

						$post_category_results = $_REQUEST['categories'];

						$postId = $post_data['id'];
						
						foreach ($post_category_results as $key => $value) {
					
							$postCategory = PostCategory::createFromArray($post_category_results);
							$postCategory->post_id = $postId;
							$postCategory->category_id = $value;
							$postCategory->save();

						}

					}

					if($check){
						$file_upload_data->uploadFileData();
					}

					$post = Post::fetch("id", $post_data['id']);

					if( $check && !empty($file_upload_data->getFileName()) ){
						$post->image = $file_upload_data->getFileName();
					}

					$post->updateFromArray($post_data);
					$record_data = $post->save();

					if($record_data){
						$message = "Successfully saved.";
					} else {
						$message = "Failed to save.";
					}

				}

				break;

		}

	}

	$posts = Post::fetchAll();

	$smarty->assign("posts", $posts);
	$smarty->display("admin/admin_posts.tpl");

?>