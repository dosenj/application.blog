<?php

function smarty_function_show_pagination($params, &$smarty) {
	
	
	//-- get parameters
	$total=$params['total'];
	$per_page=$params['per_page'];
	$current=$params['current'] + 1;	// da krece brojanje od prve strane
	$url_dodatak=$params['url_dodatak'];
	
	if (empty($url_dodatak))
		$href_osnova="?";				// ovim pocinje link .. ime_skripta.php?(ostali paramateri..)
	else
		$href_osnova="?$url_dodatak";		// ako ima dodatka na url, onda ga upisemo na pocetku pa url sad izgleda ime_skripta.php?w=1&(ostali parametri..)
	
	$result="";
	
	$num_pages=floor(($total) / $per_page);				// broj strana, zaokruzen na dole
	if (($total-$per_page) % $per_page) $num_pages++;				// ako 'preliva', tj ima ostatka, onda dodaj jos jednu stranu

	if ($num_pages<2) return;		// ako nema vise od jedne strane, ne ispisi nista

	for ($i=1; $i<=$num_pages; $i++) {
		$start=($i-1) * $per_page;
		$href=$href_osnova."start=$start";					// dakle stranica 2 pocinje sa 11
		//$href=$href_osnova."industry=$industry_id&region=$region_id&start=$start";
		if ($current == $i)
			$result.="$i ";
		else
			$result.="<a href='$href'>$i</a> ";
	
	}
	
	
	//-- add << previous | next >>
	
	$prev_next_dodatak="";
	
	if ($current > 1) {
		$start=($current-1-1) * $per_page;
		$href=$href_osnova."start=$start";
		$prev_dodatak="<a href='$href'><< </a> &nbsp; ";
	}
	//$prev_next_dodatak.="&nbsp; ";
	
	if ($current < $num_pages) {
		$start=($current-1+1) * $per_page;
		$href=$href_osnova."start=$start";
		$next_dodatak.="&nbsp;<a href='$href'> >></a>";
	}
	
	//$result="<div style='padding:6px 0px;; font-weight:bold'>".$prev_next_dodatak."</div>".$result;
	
	$result=$prev_dodatak.$result.$next_dodatak;
	
	return $result;
}

?>
