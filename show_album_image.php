<?php

	include_once "includes/inc.globals.php";
	include_once "includes/inc.front.php";

	$pages = Page::fetchAll("published='yes'");

	$categories = Category::fetchAll("published='true'");

	$albums = Album::fetchAll("published='yes'");

	$albumId = $_REQUEST['id'];

	$albumData = Image::fetchAll("album_id='".$albumId."' AND published='yes'");

	$getAlbum = Album::fetch("id", $albumId);

	$smarty->assign("pages", $pages);
	$smarty->assign("categories", $categories);
	$smarty->assign("albums", $albums);
	$smarty->assign("albumData", $albumData);
	$smarty->assign("getAlbum", $getAlbum);
	$smarty->display("show_album_image.tpl");

?>