{include file="site_header.tpl"}

	<div class="container">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">

	  		<a class="navbar-brand" href="index.php"><font size="7"><b>Bis</b><font color="blue">Lite</font></font></a>                                                  
	  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    	<span class="navbar-toggler-icon"></span>
	  		</button>

	  		<div class="collapse navbar-collapse" id="navbarSupportedContent">
	    		<ul class="navbar-nav ml-auto">
	      			{foreach $pages as $page}
	      				<li class="nav-item">
	        				<a class="nav-link" href="show_page.php?id={$page->id}" target="_blank"><b>{$page->title}</b></a>
	      				</li>
	      			{/foreach}
	      			<li class="nav-item dropdown">
	        			<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	          				<b>Categories</b>
	        			</a>
	        			<div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          				{foreach $categories as $category}
	          					<a class="dropdown-item" href="show_post_category.php?id={$category->id}" target="_blank">{$category->name}</a>
	          				{/foreach}
	        			</div>
	      			</li>
	      			<li class="nav-item dropdown">
	        			<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	          				<b>Albums</b>
	        			</a>
	        			<div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          				{foreach $albums as $album}
	          					<a class="dropdown-item" href="show_album_image.php?id={$album->id}" target="_blank">{$album->name}</a>
	          				{/foreach}
	        			</div>
	      			</li>
	    		</ul>
	  		</div>

		</nav>
	</div>

	<div style="background-color: lightblue;">
		<div class="container-fluid">
			<div class="container">
				
				<div id="sliderControls" class="carousel slide" data-ride="carousel" style="padding-top: 30px; padding-bottom: 30px;">
  					<div class="carousel-inner">

    					<div class="carousel-item active">
      						<img class="d-block w-100" src="/uploads/sliders/ddd.jpg" alt="First slide" width="100%" height="400px">
    					</div>
    					
    					{foreach $sliders as $slider}
    						<div class="carousel-item">
    							<a href="{$slider->url}" target="_blank">
      								<img class="d-block w-100" src="/uploads/sliders/{$slider->image}" alt="{$slider->filename}" width="100%" height="400px">
      							</a>
    						</div>
    					{/foreach}
    					  					
    				</div>
  					<a class="carousel-control-prev" href="#sliderControls" role="button" data-slide="prev">
    					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
    					<span class="sr-only">Previous</span>
  					</a>
  					<a class="carousel-control-next" href="#sliderControls" role="button" data-slide="next">
    					<span class="carousel-control-next-icon" aria-hidden="true"></span>
    					<span class="sr-only">Next</span>
  					</a>
				</div>

			</div>
		</div>
	</div>

	<div style="background-color: lightgrey;">
		<div class="container-fluid">
			<div class="container">
				<div class="row">
					{while $latest_post = mysqli_fetch_object($result)}
						<div class="col-sm"> 
							<h4 style="padding-top: 30px; padding-left: 10px;">{$latest_post->title}</h4>
							<div style="padding-left: 10px;">
								<p>{$latest_post->text}</p>
							</div>
							<div style="padding-top: 110px; padding-left: 10px;"> 	
								<a href="show_post.php?id={$latest_post->id}" target="_blank"><font color="black">Read more</font></a>  
							</div>
						</div>
						<div class="col-sm"> 
							<img src="/uploads/posts/{$latest_post->image}" width="300px" height="300px" style="margin-left: 270px; padding-top: 30px; padding-bottom: 30px;" />  
						</div>
					{/while}
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
    		{foreach $posts as $post}
    			<div class="col" style="margin-top: 20px;">
    				<div>
    					<img src="/uploads/posts/{$post->image}" width="540px" height="400px" />
    				</div>
    				<div>
    					<h5>{$post->title}</h5>
    				</div>
    				<div>
    					<p>{$post->text}</p>
    					<a href="show_post.php?id={$post->id}" target="_blank"><font color="black">Read more</font></a>
    					<hr />
    				</div>
    			</div>
    		{/foreach}
  		</div>
	</div>

	<div style="background-color: lightblue;">
		<div class="container-fluid">
			<div class="container" style="padding-top: 50px; padding-bottom: 10px;">
				
				<div class="flexslider carousel">
			  		<ul class="slides">
			    		{foreach $images as $image}
			    			<li>
			      				<img src="/uploads/images/{$image->image}" />
			    			</li>
			    		{/foreach}
			  		</ul>
				</div>

			</div>
		</div>
	</div>

	<script type="text/javascript">
		
		$(window).on('load', function(){
			$('.flexslider').flexslider({
    			animation: "slide",
    			animationLoop: false,
    			itemWidth: 210,
    			maxItems: 4,
    			itemMargin: 5
  			});
		});

	</script>

{include file="site_footer.tpl"}         