<!doctype html>
<html lang="en">
	<head>
		<title>Site</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<script src="../js/vendor/jquery-3.3.1.min.js" type="text/javascript"></script>
		<link rel="stylesheet" type="text/css" href="../flexslider/flexslider.css">
		<script type="text/javascript" src="../flexslider/jquery.flexslider.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

		<style>
			.fa {
		  		padding: 20px;
		  		font-size: 30px;
		  		width: 50px;
		  		text-align: center;
		  		text-decoration: none;
		  		margin: 5px 2px;
			}
			.fa:hover {
    			opacity: 0.7;
			}
			.fa-facebook {
  				background: #3B5998;
  				color: white;
			}
			.fa-dribbble {
  				background: #ea4c89;
  				color: white;
			}
			.fa-pinterest {
  				background: #cb2027;
  				color: white;
			}
			.fa-linkedin {
  				background: #007bb5;
  				color: white;
			}
			.fa-skype {
  				background: #00aff0;
  				color: white;
			}
			.fa-twitter {
  				background: #55ACEE;
  				color: white;
			}
		</style>

	</head>
	<body>