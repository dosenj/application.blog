{include file="admin/a_header.tpl"}
	<h2 align="center">Edit Post </h2>
	<form id="edit_post" name="edit_post" method="post" action="admin_posts.php" enctype="multipart/form-data">
		<table width="400" border="0" align="center" cellpadding="5" cellspacing="5" class="edit_table">
			<tr>
				<td>ID:</td>
				<td>{$post->id}</td>
			</tr>
			<tr>
				<td>User ID:</td>
				<td>{$post->user_id}</td>
			</tr>
			<tr>
				<td>Title:</td>
				<td><input type="text" name="post[title]" id="post[title]" value="{$post->title}" style="width: 96.5%;" /></td>
			</tr>
			<tr>
				<td>Text:</td>
				<td><textarea name="post[text]" id="editor">{$post->text}</textarea></td>
			</tr>
			<tr>
				<td>Published:</td>
				<td>
					<input type="radio" name="post[published]" id="post[published]" value="true" {if ($post->published == 'true') } {$check} {/if} />Published
					<br />
					<input type="radio" name="post[published]" id="post[published]" value="false" {if ($post->published == 'false') } {$check} {/if} />Not published
				</td>
			</tr>
			<tr>
				<td>Image:</td>
				<td>
					<input type="file" name="image" />
					<img src="/uploads/posts/{$post->image}" width="50px" height="50px" style="margin-top: 10px;">
				</td>
			</tr>
			<tr>
				<td>Category:</td>
				<td>
					{foreach $categories as $category}

						<input type="checkbox" name="categories[]" id="{$category->id}" value="{$category->id}" 

							{foreach $postCategories as $postCategory } 

								{if $category->id == $postCategory->category_id } {$check} {/if}

							{/foreach} 

						/>{$category->name}<br />

					{/foreach}
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" name="Submit" value="Submit" class="button_blue" />
					<input type="button" name="Submit2" value="Cancel" onclick="javascript:document.location='admin_posts.php'" />
					<input type="hidden" name="Action" id="Action" value="edit_post" />
					<input type="hidden" name="post[id]" id="post[id]" value="{$post->id}" />
				</td>
			</tr>
		</table>
	</form>
	<script type="text/javascript">
		CKEDITOR.replace('editor');
	</script>
{include file="admin/a_footer.tpl"}