{include file="admin/a_header.tpl"}
	<div align="center">
		<form name="album" id="album" method="post" action="">
			<table width="400" border="0" align="center">
				<tr>
					<td colspan="2">
						<div align="left">
							[ <a href="add_album.php">add</a> ]
						</div>
					</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr class="table_header">
					<td width="45">id</td>
      				<td width="122">name</td>
      				<td width="122">description</td>
      				<td width="122">published</td>
      				<td width="70" class="td_edit">edit</td>
      				<td width="63" class="td_delete">delete</td>
				</tr>
				{foreach $albums as $album}
					<tr class="table_row">
						<td>{$album->id}</td>
						<td>{$album->name}</td>
						<td>{$album->description}</td>
						<td>{$album->published}</td>
						<td><a href="edit_album.php?id={$album->id}">edit</a></td>
						<td><input type="checkbox" name="delete[]" id="delete" value="{$album->id}" /></td>
					</tr>
				{/foreach}
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan="2">
						<div align="right">
							<input name="Action" type="hidden" id="Action" value="delete" />
							<input type="submit" class="button_red" name="button" id="button" value="Delete selected" />
						</div>
					</td>
				</tr>
			</table>
		</form>
	</div>
{include file="admin/a_footer.tpl"}