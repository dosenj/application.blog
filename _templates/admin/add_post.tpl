{include file="admin/a_header.tpl"}
	<h2 align="center">Add Post </h2>
	<form id="add_post" name="add_post" method="post" action="admin_posts.php" enctype="multipart/form-data"> 
		<table width="400" border="0" align="center" cellpadding="5" cellspacing="5" class="edit_table">
			<tr>
				<td>Title:</td>
				<td><input type="text" name="post[title]" id="post[title]" value="" style="width: 96.5%;" /></td> 
			</tr>
			<tr>
				<td>Text:</td>
				<td><textarea id="editor" name="post[text]"></textarea></td>
			</tr>
			<tr>
				<td>Published:</td>
				<td>
					<input type="radio" name="post[published]" id="post[published]" value="true" />Published
					<br />
					<input type="radio" name="post[published]" id="post[published]" value="false" />Not published
				</td>
			</tr>
			<tr>
				<td>Image:</td>
				<td><input type="file" name="image" /></td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" name="Submit" class="button_blue" value="Submit" />
					<input type="button" name="Submit2" value="Cancel" onclick="javascript:document.location='admin_posts.php'" />
					<input type="hidden" name="Action" id="Action" value="add_post" />
				</td>
			</tr>
		</table>
	</form>
	<script type="text/javascript">
		CKEDITOR.replace('editor');
	</script>
{include file="admin/a_footer.tpl"}       