{include file="admin/a_header.tpl"}
	<div align="center">
		<form id="slider" name="slider" method="post" action="">
			<table width="400" border="0" align="center">
				<tr>
					<td colspan="2">
						<div align="left">
							[ <a href="add_slider.php">add</a> ]
						</div>
					</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr class="table_header">
					<td width="45">id</td>
      				<td width="122">filename</td>
      				<td width="122">caption</td>
      				<td width="122">published</td>
      				<td width="122">image</td>
      				<td width="122">url</td>
      				<td width="70" class="td_edit">edit</td>
      				<td width="63" class="td_delete">delete</td>
				</tr>
				{foreach $sliders as $slider}
					<tr class="table_row">
						<td>{$slider->id}</td>
						<td>{$slider->filename}</td>
						<td>{$slider->caption}</td>
						<td>{$slider->published}</td>
						<td>{$slider->image}</td>
						<td>{$slider->url}</td>
						<td><a href="edit_slider.php?id={$slider->id}">edit</a></td>
						<td><input type="checkbox" name="delete[]" id="delete" value="{$slider->id}" /></td>
					</tr>
				{/foreach}
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td> 
					<td colspan="2">
						<div align="right">
							<input name="Action" type="hidden" id="Action" value="delete" />
							<input type="submit" class="button_red" name="button" id="button" value="Delete selected" />
						</div>
					</td>
				</tr>
			</table>
		</form>
	</div>
{include file="admin/a_footer.tpl"}   