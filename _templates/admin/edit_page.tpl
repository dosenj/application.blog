{include file="admin/a_header.tpl"}
	<h2 align="center">Edit Page </h2>
	<form id="edit_page" name="edit_page" method="post" action="admin_pages.php">
		<table width="400" border="0" align="center" cellpadding="5" cellspacing="5" class="edit_table">
			<tr>
				<td>ID:</td>
				<td>{$page->id}</td>
			</tr>
			<tr>
				<td>Title:</td>
				<td><input type="text" name="page[title]" id="page[title]" value="{$page->title}" style="width: 96.5%;" /></td>
			</tr>
			<tr>
				<td>Text:</td>
				<td><textarea id="editor" name="page[text]">{$page->text}</textarea></td>
			</tr>
			<tr>
				<td>Published:</td>
				<td>
					<input type="radio" name="page[published]" id="page[published]" value="yes" {if ($page->published == 'yes') } {$check} {/if} />Yes
					<br />
					<input type="radio" name="page[published]" id="page[published]" value="no" {if ($page->published == 'no') } {$check} {/if} />No
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" name="Submit" value="Submit" class="button_blue" />
					<input type="button" name="Submit2" value="Cancel" onclick="javascript:document.location='admin_pages.php'" />
					<input type="hidden" name="Action" id="Action" value="edit_page" />
					<input type="hidden" name="page[id]" id="page[id]" value="{$page->id}" />
				</td>
			</tr>
		</table>
	</form>
	<script type="text/javascript">
		CKEDITOR.replace('editor');
	</script>
{include file="admin/a_footer.tpl"}  