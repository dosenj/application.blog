{include file="admin/a_header.tpl"}
	<h2 align="center">Add Album </h2>
	<form id="add_album" name="add_album" method="post" action="admin_albums.php">
		<table width="400" border="0" align="center" cellpadding="5" cellspacing="5" class="edit_table">
			<tr>
				<td>Name:</td>
				<td><input type="text" name="album[name]" id="album[name]" value="" /></td>
			</tr>
			<tr>
				<td>Description:</td>
				<td><input type="text" name="album[description]" id="album[description]" value="" /></td>
			</tr>
			<tr>
				<td>Published:</td>
				<td>
					<input type="radio" name="album[published]" id="album[published]" value="yes" />Yes
					<br />
					<input type="radio" name="album[published]" id="album[published]" value="no" />No
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" name="Submit" class="button_blue" value="Submit" />
					<input type="button" name="Submit2" value="Cancel" onclick="javascript:document.location='admin_albums.php'" />
					<input type="hidden" name="Action" id="Action" value="add_album" />
				</td>
			</tr>
		</table>
	</form>
{include file="admin/a_footer.tpl"}