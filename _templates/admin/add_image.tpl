{include file="admin/a_header.tpl"}
	<h2 align="center">Add Image </h2>
	<form id="add_image" name="add_image" method="post" action="admin_images.php" enctype="multipart/form-data">
		<table width="400" border="0" align="center" cellpadding="5" cellspacing="5" class="edit_table">
			<tr>
				<td>Filename:</td>
				<td><input type="text" name="image[filename]" id="image[filename]" value="" /></td>
			</tr>
			<tr>
				<td>Description:</td>
				<td><input type="text" name="image[description]" id="image[description]" value="" /></td>
			</tr>
			<tr>
				<td>Published:</td>
				<td>
					<input type="radio" name="image[published]" id="image[published]" value="yes" />Yes
					<br />
					<input type="radio" name="image[published]" id="image[published]" value="no" />No
				</td>
			</tr>
			<tr>
				<td>Album:</td>
				<td>
					<select name="image[album]">
						{foreach $albums as $album}
							<option value="{$album->id}">{$album->name}</option>
						{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td>Image:</td>
				<td><input type="file" name="image" /></td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" name="Submit" class="button_blue" value="Submit" />
					<input type="button" name="Submit2" value="Cancel" onclick="javascript:document.location='admin_images.php'" />
					<input type="hidden" name="Action" id="Action" value="add_image" />
				</td>
			</tr>
		</table>
	</form>
{include file="admin/a_footer.tpl"}