{include file="admin/a_header.tpl"}
	<h2 align="center">Edit Slider </h2>
	<form id="edit_slider" name="edit_slider" method="post" action="admin_sliders.php" enctype="multipart/form-data">
		<table width="400" border="0" align="center" cellpadding="5" cellspacing="5" class="edit_table">
			<tr>
				<td>ID:</td>
				<td>{$slider->id}</td>
			</tr>
			<tr>
				<td>Filename:</td>
				<td><input type="text" name="slider[filename]" id="slider[filename]" value="{$slider->filename}" /></td>
			</tr>
			<tr>
				<td>Caption:</td>
				<td><input type="text" name="slider[caption]" id="slider[caption]" value="{$slider->caption}" /></td>
			</tr>
			<tr>
				<td>Published:</td>
				<td>
					<input type="radio" name="slider[published]" id="slider[published]" value="yes" {if $slider->published == 'yes' } {$check} {/if} />Yes
					<br />
					<input type="radio" name="slider[published]" id="slider[published]" value="no" {if $slider->published == 'no' } {$check} {/if} />No
				</td>
			</tr>
			<tr>
				<td>Image:</td>
				<td>
					<input type="file" name="image" />
					<img src="/uploads/sliders/{$slider->image}" width="50px" height="50px" style="margin-top: 10px;">
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" name="Submit" value="Submit" class="button_blue" />
					<input type="button" name="Submit2" value="Cancel" onclick="javascript:document.location='admin_sliders.php'" />
					<input type="hidden" name="Action" id="Action" value="edit_slider" />
					<input type="hidden" name="slider[id]" id="slider[id]" value="{$slider->id}" />
				</td>
			</tr>
		</table>
	</form>
{include file="admin/a_footer.tpl"}