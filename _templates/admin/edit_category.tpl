{include file="admin/a_header.tpl"}
	<h2 align="center">Edit Category </h2>
	<form method="post" action="admin_categories.php" id="edit_category" name="edit_category">
		<table width="400" border="0" align="center" cellpadding="5" cellspacing="5" class="edit_table">
			<tr>
				<td>ID:</td>
				<td>{$category->id}</td>
			</tr>
			<tr>
				<td>Name:</td>
				<td><input type="text" name="category[name]" value="{$category->name}" /></td>
			</tr>
			<tr>
				<td>Published:</td>
				<td>
					<input type="radio" name="category[published]" value="true" {if ($category->published == 'true')} {$check} {/if} />Published
					<br />
					<input type="radio" name="category[published]" value="false" {if ($category->published == 'false')} {$check} {/if} />Not published
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" name="Submit" value="Submit" class="button_blue" />
					<input type="button" name="Submit2" value="Cancel" onclick="javascript:document.location='admin_categories.php'" />
					<input type="hidden" name="Action" id="Action" value="edit_category" />
					<input type="hidden" name="category[id]" id="category[id]" value="{$category->id}" />
				</td>
			</tr>
		</table>
	</form>
{include file="admin/a_footer.tpl"} 