{include file="admin/a_header.tpl"}
	<div align="center">
		<form id="image" name="image" method="post" action="">
			<table width="400" border="0" align="center">
				<tr>
					<td colspan="2">
						<div align="left">
							[ <a href="add_image.php">add</a> ]
						</div>
					</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr class="table_header">
					<td width="45">id</td>
      				<td width="226">album id</td>
      				<td width="122">filename</td>
      				<td width="122">description</td>
      				<td width="122">published</td>
      				<td width="122">image</td>
      				<td width="70" class="td_edit">edit</td>
      				<td width="63" class="td_delete">delete</td>
				</tr>
				{foreach $images as $image}
					<tr class="table_row">
						<td>{$image->id}</td>
						<td>{$image->album_id}</td>
						<td>{$image->filename}</td>
						<td>{$image->description}</td>
						<td>{$image->published}</td>
						<td>{$image->image}</td>
						<td><a href="edit_image.php?id={$image->id}">edit</a></td>
						<td><input type="checkbox" name="delete[]" id="delete" value="{$image->id}" /></td>
					</tr>
				{/foreach}
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan="2">
						<div align="right">
							<input type="hidden" name="Action" id="Action" value="delete" />
							<input type="submit" class="button_red" name="button" id="button" value="Delete selected" />
						</div>
					</td>
				</tr>
			</table>
		</form>
	</div>
{include file="admin/a_footer.tpl"}