{include file="admin/a_header.tpl"}
	<h2 align="center">Edit Album </h2>
	<form id="edit_album" name="edit_album" method="post" action="admin_albums.php">
		<table width="400" border="0" align="center" cellpadding="5" cellspacing="5" class="edit_table">
			<tr>
				<td>ID:</td>
				<td>{$album->id}</td>
			</tr>
			<tr>
				<td>Name:</td>
				<td><input type="text" name="album[name]" id="album[name]" value="{$album->name}" /></td>
			</tr>
			<tr>
				<td>Description:</td>
				<td><input type="text" name="album[description]" id="album[description]" value="{$album->description}" /></td>
			</tr>
			<tr>
				<td>Published:</td>
				<td>
					<input type="radio" name="album[published]" id="album[published]" value="yes" {if $album->published == 'yes' } {$check} {/if} />Yes
					<br />
					<input type="radio" name="album[published]" id="album[published]" value="no" {if $album->published == 'no' } {$check} {/if} />No
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" name="Submit" value="Submit" class="button_blue" />
					<input type="button" name="Submit2" value="Cancel" onclick="javascript:document.location='admin_albums.php'" />
					<input type="hidden" name="Action" id="Action" value="edit_album" />
					<input type="hidden" name="album[id]" id="album[id]" value="{$album->id}" />
				</td>
			</tr>
		</table>
	</form>
{include file="admin/a_footer.tpl"}