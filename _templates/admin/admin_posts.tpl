{include file="admin/a_header.tpl"}
	<div align="center">
		<form id="post" name="post" method="post" action="">
			<table width="400" border="0" align="center">
				<tr>
					<td colspan="2">
						<div align="left">
							[ <a href="add_post.php">add</a> ]
						</div>
					</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr class="table_header">
					<td width="45">id</td>
      				<td width="226">user id</td>
      				<td width="122">title</td>
      				<td width="122">text</td>
      				<td width="122">published</td>
      				<td width="122">image</td>
      				<td width="122">created</td>
      				<td width="70" class="td_edit">edit</td>
      				<td width="63" class="td_delete">delete</td>
				</tr>
				{foreach $posts as $post}
					<tr class="table_row">
						<td>{$post->id}</td>
						<td>{$post->user_id}</td>
						<td>{$post->title}</td>
						<td>{$post->text}</td>
						<td>{$post->published}</td>
						<td>{$post->image}</td>
						<td>{$post->created}</td>
						<td><a href="edit_post.php?id={$post->id}">edit</a></td>
						<td><input type="checkbox" name="delete[]" id="delete" value="{$post->id}" /></td>
					</tr>
				{/foreach}
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td> 
					<td colspan="2">
						<div align="right">
							<input name="Action" type="hidden" id="Action" value="delete" />
							<input type="submit" class="button_red" name="button" id="button" value="Delete selected" />
						</div>
					</td>
				</tr>
			</table>
		</form>
	</div>
{include file="admin/a_footer.tpl"}   