{include file="admin/a_header.tpl"}
	<h2 align="center">Add Slider </h2>
	<form id="add_slider" name="add_slider" method="post" action="admin_sliders.php" enctype="multipart/form-data">
		<table width="400" border="0" align="center" cellpadding="5" cellspacing="5" class="edit_table">
			<tr>
				<td>Filename:</td>
				<td><input type="text" name="slider[filename]" id="slider[filename]" value="" /></td>
			</tr>
			<tr>
				<td>Caption:</td>
				<td><input type="text" name="slider[caption]" id="slider[caption]" value="" /></td>
			</tr>
			<tr>
				<td>Published:</td>
				<td>
					<input type="radio" name="slider[published]" id="slider[published]" value="yes" />Yes
					<br />
					<input type="radio" name="slider[published]" id="slider[published]" value="no" />No
				</td>
			</tr>
			<tr>
				<td>Image:</td>
				<td><input type="file" name="image"></td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" name="Submit" class="button_blue" value="Submit" />
					<input type="button" name="Submit2" value="Cancel" onclick="javascript:document.location='admin_sliders.php'" />
					<input type="hidden" name="Action" id="Action" value="add_slider" />
				</td>
			</tr>
		</table>
	</form>
{include file="admin/a_footer.tpl"}