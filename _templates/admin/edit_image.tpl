{include file="admin/a_header.tpl"}
	<h2 align="center">Edit Image </h2>
	<form id="edit_image" name="edit_image" method="post" action="admin_images.php" enctype="multipart/form-data">
		<table width="400" border="0" align="center" cellpadding="5" cellspacing="5" class="edit_table">
			<tr>
				<td>ID:</td>
				<td>{$image->id}</td>
			</tr>
			<tr>
				<td>Album ID:</td>
				<td>{$image->album_id}</td>
			</tr>
			<tr>
				<td>Filename:</td>
				<td><input type="text" name="image[filename]" id="image[filename]" value="{$image->filename}" /></td>
			</tr>
			<tr>
				<td>Description:</td>
				<td><input type="text" name="image[description]" id="image[description]" value="{$image->description}" /></td>
			</tr>
			<tr>
				<td>Published:</td>
				<td>
					<input type="radio" name="image[published]" id="image[published]" value="yes" {if $image->published == 'yes' } {$check} {/if} />Yes
					<br />
					<input type="radio" name="image[published]" id="image[published]" value="no" {if $image->published == 'no' } {$check} {/if} />No
				</td>
			</tr>
			<tr>
				<td>Album:</td>
				<td>
					<select name="image[album]">
						{foreach $albums as $album}
							<option value="{$album->id}" {if $image->album_id == $album->id } {$select} {/if} >{$album->name}</option>
						{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td>Image:</td>
				<td>
					<input type="file" name="image" />
					<img src="/uploads/images/{$image->image}" width="50px" height="50px" style="margin-top: 10px;" />
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" name="Submit" value="Submit" class="button_blue" />
					<input type="button" name="Submit2" value="Cancel" onclick="javascript:document.location='admin_images.php'" />
					<input type="hidden" name="Action" id="Action" value="edit_image" />
					<input type="hidden" name="image[id]" id="image[id]" value="{$image->id}" />
				</td>
			</tr>
		</table>
	</form>
{include file="admin/a_footer.tpl"}