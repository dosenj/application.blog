{include file="site_header.tpl"}
	
	<div class="container">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">

	  		<a class="navbar-brand" href="index.php"><font size="7"><b>Bis</b><font color="blue">Lite</font></font></a>                                                  
	  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    	<span class="navbar-toggler-icon"></span>
	  		</button>

	  		<div class="collapse navbar-collapse" id="navbarSupportedContent">
	    		<ul class="navbar-nav ml-auto">
	      			{foreach $pages as $page}
	      				<li class="nav-item">
	        				<a class="nav-link" href="show_page.php?id={$page->id}" target="_blank"><b>{$page->title}</b></a>
	      				</li>
	      			{/foreach}
	      			<li class="nav-item dropdown">
	        			<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	          				<b>Categories</b>
	        			</a>
	        			<div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          				{foreach $categories as $category}
	          					<a class="dropdown-item" href="show_post_category.php?id={$category->id}" target="_blank">{$category->name}</a>
	          				{/foreach}
	        			</div>
	      			</li>
	      			<li class="nav-item dropdown">
	        			<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	          				<b>Albums</b>
	        			</a>
	        			<div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          				{foreach $albums as $album}
	          					<a class="dropdown-item" href="show_album_image.php?id={$album->id}" target="_blank">{$album->name}</a>
	          				{/foreach}
	        			</div>
	      			</li>
	    		</ul>
	  		</div>

		</nav>
	</div>

	<div class="container" style="margin-top: 20px;">
		<h3>Page data:</h3>
		<div class="media">
			<div class="media-body">
				<h5 class="mt-0">{$pageData->title}</h5>
				{$pageData->text}
			</div>
		</div>
	</div>

{include file="site_footer.tpl"}