<?php

	include_once "includes/inc.globals.php";
	include_once "includes/inc.front.php";

	$pages = Page::fetchAll("published='yes'");

	$categories = Category::fetchAll("published='true'");

	$albums = Album::fetchAll("published='yes'");

	$sliders = Slider::fetchAll("published='yes'");

	$posts = Post::fetchAll("published='true'");

	$images = Image::fetchAll("published='yes'");

	$connect = mysqli_connect("localhost", "root", "aurora", "app_blog");
	$sql = "SELECT * FROM posts WHERE published='true' ORDER BY id DESC LIMIT 1";
	$result = mysqli_query($connect, $sql);

	$smarty->assign("result", $result);

	$smarty->assign("pages", $pages);
	$smarty->assign("categories", $categories);
	$smarty->assign("albums", $albums);
	$smarty->assign("sliders", $sliders);
	$smarty->assign("posts", $posts);
	$smarty->assign("images", $images);

	$smarty->display("index.tpl");

?>