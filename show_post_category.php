<?php

	include_once "includes/inc.globals.php";
	include_once "includes/inc.front.php";

	$selected_category_id = $_REQUEST['id'];

	$connection = mysqli_connect("localhost", "root", "aurora", "app_blog");

	$sql = "SELECT posts.id AS postID, posts.title AS postTitle, posts.text AS postContent, posts.image AS postImage, categories.id AS categoryID, categories.name AS categoryName
			FROM posts
			INNER JOIN post_category ON posts.id=post_category.post_id
			INNER JOIN categories ON post_category.category_id=categories.id
			WHERE categories.id='".$selected_category_id."';";

	$results = mysqli_query($connection, $sql);	

	$smarty->assign("results", $results);

	$pages = Page::fetchAll("published='yes'");

	$categories = Category::fetchAll("published='true'");

	$albums = Album::fetchAll("published='yes'");

	$smarty->assign("pages", $pages);
	$smarty->assign("categories", $categories);
	$smarty->assign("albums", $albums);

	$smarty->display("show_post_category.tpl");

?>