<?php

	include_once "includes/inc.globals.php";
	include_once "includes/inc.front.php";

	$pages = Page::fetchAll("published='yes'");

	$categories = Category::fetchAll("published='true'");

	$albums = Album::fetchAll("published='yes'");

	$postId = $_REQUEST['id'];

	$post = Post::fetch("id", $postId);

	$smarty->assign("post", $post);
	$smarty->assign("pages", $pages);
	$smarty->assign("categories", $categories);
	$smarty->assign("albums", $albums);
	$smarty->display("show_post.tpl");

?>